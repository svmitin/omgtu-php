<?php

$url = $_SERVER['REQUEST_URI'];
$category = $_GET["category"];
$book = $_GET["book"];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Книги по программированию</title>
    <link rel="stylesheet" href="styles.css" />
    <link rel="icon" type="image/png" href="img/favicon.png">
</head>
<body>

<?php
// Главное меню
include "_header.php"
?>

<div id="content">
        <?php
            // Шаблон главной страницы
            if (!$category && !$book){
                include "main_content.php";
            }

            // Книги заданной категории
            if ($category && !$book) {
                include "category.php";
            }

            // Конкретная книга
            if ($category && $book) {
                include "book.php";
            }
        ?>
</div>
<!-- end content -->

<?php
include "_footer.php"
?>

</body>
</html>