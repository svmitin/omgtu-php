###Сайт библиотеки

* Зарегистрируйтесь на DigitalOcean
* Создайте новый дроплет на Ubuntu 21.04
* Подключитесь к дроплету по SSH
* Выполните команды
  
```
apt-get install sqlite nginx
apt-get install php7.4 php7.4-sqlite php7.4-cli php7.4-common php7.4-json php7.4-opcache php7.4-mysql php7.4-mbstring php7.4-zip php7.4-fpm
```
* В фале `/etc/php/7.4/cli/php.ini` измените параметр `cgi.fix_pathinfo` на 0

```
nano /etc/php/7.4/cli/php.ini
```
* Перезапустите php

```
systemctl restart php7.4-fpm.service
```
* Заполните содержимое файла `/etc/nginx/sites-available/default`

```
server {
        listen 80;
        server_name svmitin_books_for_omgtu.ru;
        root /var/www/omgtu-php;
        index index.php;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ \.php$ {
            fastcgi_pass unix:/run/php/php7.4-fpm.sock;
            include snippets/fastcgi-php.conf;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }
        
        location /img {
            expires 1M;
            try_files $uri =404;
        }

        location /.gitignore {
            deny all;
            return 404;
        }

        location ~ /\.ht {
                deny all;
        }
}
```
* Перезапустите службы
  
```
systemctl restart nginx.service
systemctl enable nginx.service
systemctl enable php7.4-fpm.service
```
* Склонируйте проект
  
```
cd /var/www
git clone https://gitlab.com/svmitin/omgtu-php.git
chown -R www-data /var/www/omgtu-php/
```
* Проект готов к использованию!
