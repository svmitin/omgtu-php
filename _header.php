<div id="nav">
    <div id="nav_wrapper">
        IT library by DEF
        <ul>

            <li><a href="/index.php">Главная</a></li>

            <?php

                // Извлекаем из БД список категорий для отрисовки главного меню
                $db = new SQLite3('db.sqlite3');
                $sql = "SELECT * FROM category ORDER BY position";
                $result = $db->query($sql);
                $array = array();
                while ($data = $result->fetchArray(SQLITE3_ASSOC)) {
                    $array[] = $data;
                }
                foreach ($array as $row) {
                    print "<li><a href=\"index.php?category=" . $row['category'] . "\">" . $row['title'] . "</a></li>";
                }

            ?>

        </ul>
    </div>
</div>