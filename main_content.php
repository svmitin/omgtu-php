

<h1>IT библиотека</h1>
<p>На данном ресурсе вы сможете найти ссылки на уже изученную и рекомендованную к ознакомлению IT-литературу</p>
<p>Под рассмотрение попадают следующие категории</p>
<ul>
    <li>Unix системы и ОС</li>
    <li>Программирования для WEB</li>
    <li>ООП и Python</li>
    <li>Информационная безопасность</li>
</ul>
<h2>О проекте</h2>
<a href="https://html.spec.whatwg.org/multipage/" title="HTML5">
    <img class="tech_logo" src="img/html5.png"></a>
<a href="https://www.w3.org/TR/CSS/#css" title="CSS3">
    <img class="tech_logo" src="img/css3.png"></a>
<a href="https://www.php.net/" title="PHP">
    <img class="tech_logo" src="img/php.png"></a>
<a href="https://www.sqlite.org/" title="SQLite">
    <img class="tech_logo" src="img/sqlite.gif"></a>
<a href="https://nginx.org/ru" title="NGINX">
    <img class="tech_logo" src="img/nginx.png"></a>
<a href="https://letsencrypt.org/" title="Let's Encrypt">
    <img class="tech_logo" src="img/letsencrypt.png"></a>
<a href="https://ubuntu.com" title="Ubuntu">
    <img class="tech_logo" src="img/ubuntu.png"></a>
<a href="https://git-scm.com" title="git">
    <img class="tech_logo" src="img/git.png"></a>
<a href="https://gitlab.com/" title="GitLab">
    <img class="tech_logo" src="img/gitlab.png"></a>
<p><b>Стек технологий:</b> PHP7.4, HTML5, CSS3, Ubuntu 21.04, NGINX, SQLite3</p>
<p><b>Исходный код:</b> <a href="https://gitlab.com/svmitin/omgtu-php">https://gitlab.com/svmitin/omgtu-php</a> </p>
<p><b>Конфигурация PHP:</b> <a href="phpinfo.php">Посмотреть</a></p>
<p><b>Демон-владелец:</b> <?php print exec('whoami') ?></p>
<p><b>Скачать файл БД:</b> <a href="db.sqlite3">db.sqlite3</a></p>
<br><br>